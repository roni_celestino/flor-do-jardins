import { FlorjardinsPage } from './app.po';

describe('florjardins App', () => {
  let page: FlorjardinsPage;

  beforeEach(() => {
    page = new FlorjardinsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
