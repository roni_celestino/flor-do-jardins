import { CanActivateChild } from '@angular/router';
import { NgModule, ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';

//MODULO

import { SiteModule } from './site/site.module';

//GUARDA


const appRoutes: Routes = [
    { path: '', pathMatch: 'prefix', redirectTo: 'site'},
    { path: 'site', loadChildren: 'app/site/site.module#SiteModule' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]

})

export class AppRoutingModule { }