import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

//DB
import { CardapioData } from './paginas/cardapio/cardapio-data';

import {
    MdButtonModule,
    MdCheckboxModule,
    MdToolbarModule,
    MdSidenavModule,
    MdIconModule,
    MdGridListModule,
    MdInputModule,
    MdSelectModule,
    MaterialModule,
    MdAutocompleteModule
} from '@angular/material';

import { requestOptionsProvider } from './default-request-options.service';

//SERVIÇE
import { CardapioService } from './paginas/cardapio/cardapio.service';
import { SlideService } from './component/slide/slide.service';


//MODULO
import { SiteRoutingModule } from './site.routing.module';

//COMPONENTES
import { SiteComponent } from './site.component';
import { HomeComponent } from './paginas/home/home.component';
import { FooterComponent } from './component/footer/footer.component';
import { SlideComponent } from './component/slide/slide.component';
import { SobreComponent } from './paginas/sobre/sobre.component';
import { CardapioComponent } from './paginas/cardapio/cardapio.component';
import { ContatoComponent } from './paginas/contato/contato.component';
import { ImgHeaderComponent } from './component/img-header/img-header.component';
import { PizzasComponent } from './paginas/cardapio/pizzas/pizzas.component';
import { BebidasComponent } from './paginas/cardapio/bebidas/bebidas.component';
import { PratosComponent } from './paginas/cardapio/pratos/pratos.component';

@NgModule({
    imports: [
        CommonModule,
        MdButtonModule,
        MdCheckboxModule,
        MdToolbarModule,
        MdSidenavModule,
        MdIconModule,
        MdGridListModule,
        MdAutocompleteModule,
        MdInputModule,
        MdSelectModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(CardapioData),
        SiteRoutingModule,
    ],

    declarations: [
        SiteComponent,
        HomeComponent,
        FooterComponent,
        SlideComponent,
        SobreComponent,
        CardapioComponent,
        ContatoComponent,
        ImgHeaderComponent,
        PizzasComponent,
        BebidasComponent,
        PratosComponent
    ],
    exports: [
    ],
    providers: [SlideService, requestOptionsProvider, CardapioService]

})


export class SiteModule { }