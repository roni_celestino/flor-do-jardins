import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgHeaderComponent } from './img-header.component';

describe('ImgHeaderComponent', () => {
  let component: ImgHeaderComponent;
  let fixture: ComponentFixture<ImgHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
