import { NgModule, ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';

import { SiteComponent } from './site.component';
import { HomeComponent } from './paginas/home/home.component';
import { SobreComponent } from './paginas/sobre/sobre.component';
import { CardapioComponent } from './paginas/cardapio/cardapio.component';
import { ContatoComponent } from './paginas/contato/contato.component';
import { PizzasComponent } from './paginas/cardapio/pizzas/pizzas.component';
import { PratosComponent } from './paginas/cardapio/pratos/pratos.component';
import { BebidasComponent } from './paginas/cardapio/bebidas/bebidas.component';

const siteRoutes: Routes = [
    {
        path: '', component: SiteComponent, children: [
            { path: '', pathMatch: 'prefix', redirectTo: 'inicio' },
            { path: 'inicio', component: HomeComponent },
            { path: 'sobre', component: SobreComponent },
            {
                path: 'cardapio', component: CardapioComponent, children: [
                    { path: 'pizzas', component: PizzasComponent },
                    { path: 'bebidas', component: BebidasComponent },
                    { path: 'pratos', component: PratosComponent },
                ]

            },
            { path: 'contato', component: ContatoComponent }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(siteRoutes)],
    exports: [RouterModule]

})

export class SiteRoutingModule { }