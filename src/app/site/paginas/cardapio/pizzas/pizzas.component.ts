
import { Component, OnInit } from '@angular/core';

import { CardapioService } from './../cardapio.service';
import { Cardapio } from "./../cardapio";

@Component({
  selector: 'app-pizzas',
  templateUrl: './pizzas.component.html',
  styleUrls: ['./pizzas.component.css']
})
export class PizzasComponent implements OnInit {
  cardapioPizza: Cardapio[];
  errorMessage: string;

  constructor(private cardapioService: CardapioService) { }

  ngOnInit() {
    this.getCardapioPizza();
  }
  getCardapioPizza() {
    this.cardapioService.getCardapioPizza()
      .subscribe(
      data => this.cardapioPizza = data,
      error => this.errorMessage = <any>error);
  }
}
