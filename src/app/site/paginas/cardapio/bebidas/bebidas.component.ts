import { Component, OnInit } from '@angular/core';

import { CardapioService } from './../cardapio.service';
import { Cardapio } from "./../cardapio";

@Component({
  selector: 'app-bebidas',
  templateUrl: './bebidas.component.html',
  styleUrls: ['./bebidas.component.css']
})
export class BebidasComponent implements OnInit {
  cardapioVinhos: Cardapio[];
  cardapioSucosPolpa: Cardapio[];
  cardapioSucoFrutas: Cardapio[];
  cardapioBebidasAlcoolicasDoce: Cardapio[];
  cardapioBebidasSemAlcool: Cardapio[];
  cardapioRefrigerante: Cardapio[];
  cardapioCerveja600: Cardapio[];
  cardapioLongNeck: Cardapio[];
  cardapio: Cardapio;
  errorMessage: string;

  constructor(private cardapioService: CardapioService) { }

  ngOnInit() {
    this.getCardapioLongNeck();
    this.getCardapioCerveja600()
    this.getCardapioRefrigerante();
    this.getCardapioBebidasSemAlcool();
    this.getCardapioBebidasAlcoolicasDoce();
    this.getCardapioSucoFrutas();
    this.getCardapioSucosPolpa();
    this.getCardapioVinhos();
  }


  getCardapioLongNeck() {
    this.cardapioService.getCardapioLongNeck()
      .subscribe(
      data => this.cardapioLongNeck = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioCerveja600() {
    this.cardapioService.getCardapioCerveja600()
      .subscribe(
      data => this.cardapioCerveja600 = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioRefrigerante() {
    this.cardapioService.getCardapioRefrigerante()
      .subscribe(
      data => this.cardapioRefrigerante = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioBebidasSemAlcool() {
    this.cardapioService.getCardapioBebidasSemAlcool()
      .subscribe(
      data => this.cardapioBebidasSemAlcool = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioBebidasAlcoolicasDoce() {
    this.cardapioService.getCardapioBebidasAlcoolicasDoce()
      .subscribe(
      data => this.cardapioBebidasAlcoolicasDoce = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioSucoFrutas() {
    this.cardapioService.getCardapioSucoFrutas()
      .subscribe(
      data => this.cardapioSucoFrutas = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioSucosPolpa() {
    this.cardapioService.getCardapioSucosPolpa()
      .subscribe(
      data => this.cardapioSucosPolpa = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioVinhos() {
    this.cardapioService.getCardapioVinhos()
      .subscribe(
      data => this.cardapioVinhos = data,
      error => this.errorMessage = <any>error);
  }

}
