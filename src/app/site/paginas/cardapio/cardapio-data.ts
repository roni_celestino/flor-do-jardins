import { InMemoryDbService } from 'angular-in-memory-web-api';

export class CardapioData implements InMemoryDbService {
  createDb() {
    let pizzasDB = [
      { id: 1, item: 'Mussarela', ingredientes: 'Mussarela, Tomate, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 2, item: 'Calabresa', ingredientes: 'Mussarela, Calabresa Fatiada, Cebola, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 3, item: 'Atum', ingredientes: 'Atum, Tomate, Cebola, Mussarela, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 4, item: 'Toscana', ingredientes: 'Mussarela, Calabresa Moída, Champignon, Cebola, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 5, item: 'Frango com Catupiry', ingredientes: 'Frango desfiado preparado com molho especial catupiry', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 6, item: 'Portuguesa', ingredientes: 'Presunto, Ovos, Cebola, Ervilha, Palmito, Mussarela', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 7, item: 'Baiana', ingredientes: 'Mussarela, Presunto, Calabresa, Bacon, Pimenta, Calabresa, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 8, item: 'Alho', ingredientes: 'Mussarela, Tomate, Parmesão, Alho Dourado, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 9, item: 'Americana', ingredientes: 'Mussarela, Presunto, Calabresa, Bacon ao limão, Azeitona', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 10, item: 'Peru', ingredientes: 'Peito de Peru, Palmito, Tomate, Mussarela', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 11, item: 'Via', ingredientes: 'Peito de Peru, Ervilha, Catupiry, Mussarela, Manjericão Fresco', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 12, item: 'Flor do Jardins', ingredientes: 'Mussarela, Presunto, Ovos, tomate, Calabresa, Pimentão', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 13, item: 'Lombo ao Creme', ingredientes: 'Lombo, Tomate, Catupiry e Mussarela', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 14, item: 'Tomate Seco e Rúcula', ingredientes: 'Mussarela, Tomate Seco, Rúcula, Alho Dourado', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 15, item: 'Brócolis no Alho', ingredientes: 'Brócolis no Alho, Mussarela, Tomate, Provolone, Alho', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 16, item: '2 Queijos', ingredientes: 'Catupiry, Mussarela', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 17, item: '3 Queijos', ingredientes: 'Catupiry, Mussarela, Provolone', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 18, item: '4 Queijos', ingredientes: 'Catupiry, Mussarela, Provolone, Parmesão', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 19, item: '5 Queijos', ingredientes: 'Catupiry, Mussarela, Provolone, Parmesão, Gorgonzola', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 20, item: 'Marguerita', ingredientes: 'Mussarela, Tomate, Parmesão, Manjericão', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 21, item: 'Rúcula', ingredientes: 'Mussarela, Rúcula, Tomate Seco', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 22, item: 'Escarola', ingredientes: 'Mussarela, Escarola e Bacon', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 23, item: 'Camarão', ingredientes: 'Camarão preparado com molho especial', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 24, item: 'Filé Mignon ', ingredientes: '', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 25, item: 'Filé com Champignon ', ingredientes: '', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 26, item: 'Filé e Tomate Seco', ingredientes: 'Camarão preparado com molho especial', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 27, item: 'Estrogonofe de Frango ', ingredientes: 'Camarão preparado com molho especial', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 28, item: 'Abacaxi', ingredientes: 'Mussarela, Abacaxi, Açúcar, Canela ', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 29, item: 'Banana', ingredientes: '', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 30, item: 'Brigadeiro', ingredientes: '', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 30, item: 'Surpresa com Sorvete', ingredientes: '', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' },
      { id: 30, item: 'Chocolate com Morango', ingredientes: '', categoria: 'Pizza', valor: '28,90', valorDois: '38,90', valorTres: '51,90' }
    ];
    let cervejaLongNeckDB = [
      { id: 1, item: 'Budweiser', ingredientes: '', categoria: 'Cerveja Long Neck', valor: '5,50' },
      { id: 2, item: 'Stella', ingredientes: '', categoria: 'Cerveja Long Neck', valor: '6,00' },
      { id: 3, item: 'Itaipava', ingredientes: '', categoria: 'Cerveja Long Neck', valor: '5,00' },
      { id: 4, item: 'Antarctica Cristal', ingredientes: '', categoria: 'Cerveja Long Neck', valor: '6,00' },
      { id: 5, item: 'Skol', ingredientes: '', categoria: 'Cerveja Long Neck', valor: '5,00' },
      { id: 6, item: 'Brahma Malzbier', ingredientes: '', categoria: 'Cerveja Long Neck', valor: '5,00' }
    ];

    let cerveja600DB = [
      { id: 1, item: 'Antarctica Original', ingredientes: '', categoria: 'Cerveja 600 ml', valor: '8,00' },
      { id: 2, item: 'Antarctica Subzero', ingredientes: '', categoria: 'Cerveja 600 ml', valor: '5,00' },
      { id: 3, item: 'Brahma', ingredientes: '', categoria: 'Cerveja 600 ml', valor: '7,00' },
      { id: 4, item: 'Skol', ingredientes: '', categoria: 'Cerveja 600 ml', valor: '7,00' },
      { id: 5, item: 'Serramalte', ingredientes: '', categoria: 'Cerveja 600 ml', valor: '9,00' },
      { id: 6, item: 'Itaipava', ingredientes: '', categoria: 'Cerveja 600 ml', valor: '6,00' }
    ];

    let refrigeranteDB = [
      { id: 1, item: 'Lata', ingredientes: '', categoria: 'Refrigerante', valor: '4,00' },
      { id: 2, item: '2 Litros', ingredientes: '', categoria: 'Refrigerante', valor: '6,00' }
    ];

    let bebidasSemAlcoolDB = [
      { id: 1, item: 'Água Mineral c/ Gás', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '2,50' },
      { id: 2, item: 'Água Mineral s/Gás', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '2,50' },
      { id: 3, item: 'Água Copo', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '1,00' },
      { id: 4, item: 'Água de Coco', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '5,00' },
      { id: 5, item: 'Aquarius Fresh', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '3,50' },
      { id: 6, item: 'H2OH', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '3,50' },
      { id: 7, item: 'Energético Fusion', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '6,00' },
      { id: 8, item: 'Red Bull', ingredientes: '', categoria: 'Bebidas Sem Alcool', valor: '8,50' }
    ];

    let bebidasAlcoolicasDoceDB = [
      { id: 1, item: 'Pitú Aguardente ', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '2,00' },
      { id: 2, item: 'Campari', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '5,50' },
      { id: 3, item: 'Conhaque Domecq', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '4,00' },
      { id: 4, item: 'Conhaque Dreher', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '3,00' },
      { id: 5, item: 'Conhaque Macieira', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '3,00' },
      { id: 6, item: 'Martini Bianco', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '7,90' },
      { id: 7, item: 'Rum Montilla', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '3,90' },
      { id: 8, item: 'Vodka Absolut', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '11,90' },
      { id: 8, item: 'Vodka Smirnoff', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '4,90' },
      { id: 8, item: 'Whisky Teachers', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '6,90' },
      { id: 8, item: 'Whisky Ballantines', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '11,90' },
      { id: 8, item: 'Whisky Old Parr', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '15,90' },
      { id: 8, item: 'Red', ingredientes: '', categoria: 'Bebidas Alcoólicas (Doce)', valor: '11,90' }
    ];

    let sucoFrutasDB = [
      { id: 1, item: 'laranja e Limão', ingredientes: '', categoria: 'Suco de Frutas', valor: '4,00', ValorDois: '9,00' }
    ];

    let sucosPolpaDB = [
      { id: 1, item: 'Ameixa', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 2, item: 'Manga', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 3, item: 'Cajá', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 4, item: 'Cacau', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 5, item: 'Uva', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 6, item: 'Acerola', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 7, item: 'Caja', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 8, item: 'Mangaba', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 9, item: 'Goiaba', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 10, item: 'Umbu', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 11, item: 'Abacaxi', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 12, item: 'Graviola', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 13, item: 'Cupuaçu', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 14, item: 'Maracujá', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' },
      { id: 15, item: 'Abacaxi com Hortelã', ingredientes: '', categoria: 'Suco de Polpa', valor: '4,00', ValorDois: '9,00' }
    ];

    let vinhosDB = [
      { id: 1, item: 'Miolo Sel', ingredientes: '', categoria: 'Suco de Polpa', valor: '44,90' },
      { id: 2, item: 'Sta Helena', ingredientes: '', categoria: 'Suco de Polpa', valor: '52,90' },
      { id: 3, item: 'Tarapaca', ingredientes: '', categoria: 'Suco de Polpa', valor: '49,90' }
    ];

    let porcoesDB = [
      { id: 1, item: 'Feijão', ingredientes: '', categoria: 'Porções', valor: '4,90' },
      { id: 2, item: 'Arroz', ingredientes: '', categoria: 'Porções', valor: '4,90' },
      { id: 3, item: 'Farofa', ingredientes: '', categoria: 'Porções', valor: '4,90' },
      { id: 4, item: 'Vinagrete', ingredientes: '', categoria: 'Porções', valor: '4,90' }
    ];

    let caldinhosDB = [
      { id: 1, item: 'Caldinho de Feijão', ingredientes: '', categoria: 'Porções', valor: '5,90' },
      { id: 2, item: 'Caldinho de Camarão', ingredientes: '', categoria: 'Porções', valor: '7,90' },
      { id: 3, item: 'Caldinho de Sururu', ingredientes: '', categoria: 'Porções', valor: '6,90' }
    ];

    let sopasDB = [
      { id: 1, item: 'Sopa de Feijão', ingredientes: '', categoria: 'Porções', valor: '10,90' },
      { id: 2, item: 'Sopa de legumes', ingredientes: '', categoria: 'Porções', valor: '10,90' },
      { id: 3, item: 'Sopa de Canja', ingredientes: '', categoria: 'Porções', valor: '4,90' }
    ];

    return {
      pizzasDB,
      cervejaLongNeckDB,
      cerveja600DB,
      refrigeranteDB,
      bebidasSemAlcoolDB,
      bebidasAlcoolicasDoceDB,
      sucoFrutasDB,
      sucosPolpaDB,
      vinhosDB,
      porcoesDB,      
      caldinhosDB,
      sopasDB
    };
  }
}







