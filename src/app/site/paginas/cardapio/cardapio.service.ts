import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { Cardapio } from './cardapio';


@Injectable()
export class CardapioService {
  private cardapioUrl = "api";

  constructor(private http: Http) { }

  getCardapioPizza(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/pizzasDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioLongNeck(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/cervejaLongNeckDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioCerveja600(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/cerveja600DB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioRefrigerante(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/refrigeranteDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioBebidasSemAlcool(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/bebidasSemAlcoolDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioBebidasAlcoolicasDoce(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/bebidasAlcoolicasDoceDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioSucosPolpa(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/sucosPolpaDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioSucoFrutas(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/sucoFrutasDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioVinhos(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/vinhosDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioPorcoes(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/porcoesDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

    getCardapioCaldinhos(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/caldinhosDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

    getCardapioSopas(): Observable<Cardapio[]> {
    return this.http.get(this.cardapioUrl + '/sopasDB')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getCardapioFiltro(catg: string): Observable<Cardapio[]> {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    let myParams = new URLSearchParams();
    myParams.set('categoria', catg);
    let options = new RequestOptions({ headers: myHeaders, params: myParams });
    return this.http.get(this.cardapioUrl, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data;
  }
  private handleError(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

}
