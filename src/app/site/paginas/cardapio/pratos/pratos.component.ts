import { Component, OnInit } from '@angular/core';

import { CardapioService } from './../cardapio.service';
import { Cardapio } from "./../cardapio";

@Component({
  selector: 'app-pratos',
  templateUrl: './pratos.component.html',
  styleUrls: ['./pratos.component.css']
})
export class PratosComponent implements OnInit {
  cardapioPorcoes: Cardapio[];
  cardapioCaldinhos: Cardapio[];
  cardapioSopas: Cardapio[];
  errorMessage: string;

  constructor(private cardapioService: CardapioService) { }

  ngOnInit() {
    this.getCardapioPorcoes();
    this.getCardapioCaldinhos();
    this.getCardapioSopas();
  }

  getCardapioPorcoes() {
    this.cardapioService.getCardapioPorcoes()
      .subscribe(
      data => this.cardapioPorcoes = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioCaldinhos() {
    this.cardapioService.getCardapioCaldinhos()
      .subscribe(
      data => this.cardapioCaldinhos = data,
      error => this.errorMessage = <any>error);
  }

  getCardapioSopas() {
    this.cardapioService.getCardapioSopas()
      .subscribe(
      data => this.cardapioSopas = data,
      error => this.errorMessage = <any>error);
  }




}
