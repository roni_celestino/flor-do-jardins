
{
    "data":[
    { "id": 1, "pizza": "Mussarela", "ingredientes": "Mussarela, Tomate, Azeitona", "categoria": "Pizza" },
    { "id": 2, "pizza": "Calabresa", "ingredientes": "Mussarela, Calabresa Fatiada, Cebola, Azeitona", "categoria": "Pizza" },
    { "id": 3, "pizza": "Atum", "ingredientes": "Atum, Tomate, Cebola, Mussarela, Azeitona", "categoria": "Pizza" },
    { "id": 4, "pizza": "Toscana", "ingredientes": "Mussarela, Calabresa Moída, Champignon, Cebola, Azeitona", "categoria": "Pizza" },
    { "id": 5, "pizza": "Frango com Catupiry", "ingredientes": "Frango desfiado preparado com molho especial catupiry", "categoria": "Pizza" },
    { "id": 6, "pizza": "Portuguesa", "ingredientes": "Presunto, Ovos, Cebola, Ervilha, Palmito, Mussarela", "categoria": "Pizza" },
    { "id": 7, "pizza": "Baiana", "ingredientes": "Mussarela, Presunto, Calabresa, Bacon, Pimenta, Calabresa, Azeitona", "categoria": "Pizza" },
    { "id": 8, "pizza": "Alho", "ingredientes": "Mussarela, Tomate, Parmesão, Alho Dourado, Azeitona", "categoria": "Pizza" },
    { "id": 9, "pizza": "Americana", "ingredientes": "Mussarela, Presunto, Calabresa, Bacon ao limão, Azeitona", "categoria": "Pizza" },
    { "id": 10, "pizza": "Peru", "ingredientes": "Peito de Peru, Palmito, Tomate, Mussarela", "categoria": "Pizza" },
    { "id": 11, "pizza": "Via", "ingredientes": "Peito de Peru, Ervilha, Catupiry, Mussarela, Manjericão Fresco", "categoria": "Pizza" },
    { "id": 12, "pizza": "Flor do Jardins", "ingredientes": "Mussarela, Presunto, Ovos, tomate, Calabresa, Pimentão", "categoria": "Pizza" },
    { "id": 13, "pizza": "Lombo ao Creme", "ingredientes": "Lombo, Tomate, Catupiry e Mussarela", "categoria": "Pizza" },
    { "id": 14, "pizza": "Tomate Seco e Rúcula", "ingredientes": "Mussarela, Tomate Seco, Rúcula, Alho Dourado", "categoria": "Pizza" },
    { "id": 15, "pizza": "Brócolis no Alho", "ingredientes": "Brócolis no Alho, Mussarela, Tomate, Provolone, Alho", "categoria": "Pizza" },
    { "id": 16, "pizza": "2 Queijos", "ingredientes": "Catupiry, Mussarela", "categoria": "Pizza" },
    { "id": 17, "pizza": "3 Queijos", "ingredientes": "Catupiry, Mussarela, Provolone", "categoria": "Pizza" },
    { "id": 18, "pizza": "4 Queijos", "ingredientes": "Catupiry, Mussarela, Provolone, Parmesão", "categoria": "Pizza" },
    { "id": 19, "pizza": "5 Queijos", "ingredientes": "Catupiry, Mussarela, Provolone, Parmesão, Gorgonzola", "categoria": "Pizza" },
    { "id": 20, "pizza": "Marguerita", "ingredientes": "Mussarela, Tomate, Parmesão, Manjericão", "categoria": "Pizza" },
    { "id": 21, "pizza": "Rúcula", "ingredientes": "Mussarela, Rúcula, Tomate Seco", "categoria": "Pizza" },
    { "id": 22, "pizza": "Escarola", "ingredientes": "Mussarela, Escarola e Bacon", "categoria": "Pizza" },
    { "id": 23, "pizza": "Camarão", "ingredientes": "Camarão preparado com molho especial", "categoria": "Pizza" },
    { "id": 24, "pizza": "Filé Mignon ", "ingredientes": "", "categoria": "Pizza" },
    { "id": 25, "pizza": "Filé com Champignon ", "ingredientes": "", "categoria": "Pizza" },
    { "id": 26, "pizza": "Filé e Tomate Seco", "ingredientes": "Camarão preparado com molho especial", "categoria": "Pizza" },
    { "id": 27, "pizza": "Estrogonofe de Frango ", "ingredientes": "Camarão preparado com molho especial", "categoria": "Pizza" },
    { "id": 28, "pizza": "Abacaxi", "ingredientes": "Mussarela, Abacaxi, Açúcar, Canela ", "categoria": "Pizza" },
    { "id": 29, "pizza": "Banana", "ingredientes": "", "categoria": "Pizza" },
    { "id": 30, "pizza": "Brigadeiro", "ingredientes": "", "categoria": "Pizza" },
    { "id": 30, "pizza": "Surpresa com Sorvete", "ingredientes": "", "categoria": "Pizza" },
    { "id": 30, "pizza": "Chocolate com Morango", "ingredientes": "", "categoria": "Pizza" },
]
}







